package ru.gb.august_gb.pages

import androidx.core.content.res.TypedArrayUtils
import androidx.core.content.res.TypedArrayUtils.getText
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import ru.gb.august_gb.R

class DashboardPage {

    fun checkDashboardButtonIsDisplayed(): DashboardPage {
        onView(withId(R.id.navigation_dashboard))
            .check(matches(isDisplayed()))
        return this
    }

    fun checkDashboardPageHasTitle(text: String): DashboardPage {
        onView(withId(R.string.title_dashboard))
            .equals(withText(text))
        return this
    }

    fun checkDashboardPageHasText(textDashboard: String): DashboardPage {
        onView(withId(R.id.text_dashboard))
            .equals(withText((textDashboard)))
        return this
    }

    fun checkHomeButtonIsDisplayed(): DashboardPage {
        onView(withId(R.id.navigation_home))
            .check(matches(isDisplayed()))
        return this
    }

    fun checkNotificationsButtonIsDisplayed(): DashboardPage {
        onView(withId(R.id.navigation_notifications))
            .check(matches(isDisplayed()))
        return this
    }

}