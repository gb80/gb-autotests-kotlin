package ru.gb.august_gb.pages

import androidx.core.content.res.TypedArrayUtils
import androidx.core.content.res.TypedArrayUtils.getText
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import ru.gb.august_gb.R

class NotificationsPage {

    fun checkNotificationsButtonIsDisplayed(): NotificationsPage {
        onView(withId(R.id.navigation_notifications))
            .check(matches(isDisplayed()))
        return this
    }

    fun checkNotificationsPageHasTitle(text: String): NotificationsPage {
        onView(withId(R.string.title_notifications))
            .equals(withText(text))
        return this
    }

    fun checkNotificationsPageHasText(textNotifications: String): NotificationsPage {
        onView(withId(R.id.text_notifications))
            .equals(withText((textNotifications)))
        return this
    }

    fun checkHomeButtonIsDisplayed(): NotificationsPage {
        onView(withId(R.id.navigation_home))
            .check(matches(isDisplayed()))
        return this
    }

    fun checkDashboardButtonIsDisplayed(): NotificationsPage {
        onView(withId(R.id.navigation_dashboard))
            .check(matches(isDisplayed()))
        return this
    }

}