package ru.gb.august_gb.pages

import androidx.core.content.res.TypedArrayUtils.getText
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.PreferenceMatchers.withTitle
import androidx.test.espresso.matcher.ViewMatchers.*
import ru.gb.august_gb.R

class HomePage {

    fun checkHomeButtonIsDisplayed(): HomePage {
        onView(withId(R.id.navigation_home))
            .check(matches(isDisplayed()))
        return this
    }

    fun checkHomePageHasTitle(text: String): HomePage {
        onView(withId(R.string.title_home))
            .equals(withText(text))
        return this
    }

    fun checkHomePageHasText(textHome: String): HomePage {
        onView(withId(R.id.text_home))
            .equals(withText((textHome)))
        return this
    }

    fun checkDashboardButtonIsDisplayed(): HomePage {
        onView(withId(R.id.navigation_dashboard))
            .check(matches(isDisplayed()))
        return this
    }

    fun checkNotificationsButtonIsDisplayed(): HomePage {
        onView(withId(R.id.navigation_notifications))
            .check(matches(isDisplayed()))
        return this
    }
}
