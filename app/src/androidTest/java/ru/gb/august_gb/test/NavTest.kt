package ru.gb.august_gb.test

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.gb.august_gb.MainActivity
import ru.gb.august_gb.pages.DashboardPage
import ru.gb.august_gb.pages.HomePage
import ru.gb.august_gb.pages.NotificationsPage

@RunWith(AndroidJUnit4::class)
class NavTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    //Проверка наличия всех элементов меню на странице Home
    @Test
    fun checkHomeButtonIsDisplayedOnHomePage()  {
        HomePage().checkHomeButtonIsDisplayed()
    }

    @Test
    fun checkDashboardButtonIsDisplayedOnHomePage() {
        HomePage().checkDashboardButtonIsDisplayed()
    }

    @Test
    fun checkNotificationsButtonIsDisplayedOnHomePage() {
        HomePage().checkNotificationsButtonIsDisplayed()
    }


    //Проверка отображения заголовков на соответствующих им сраницах
    @Test
    fun checkHomeButtonHasText()  {
        HomePage().checkHomePageHasTitle("Home")
    }

    @Test
    fun checkDashboardButtonHasText()  {
        DashboardPage().checkDashboardPageHasTitle("Dashboard")
    }

    @Test
    fun checkNotificationsButtonHasText()  {
        NotificationsPage().checkNotificationsPageHasTitle("Notifications")
    }

    //Проверка наличия соответствующего текста на каждой странице
    @Test
    fun checkHomePageHasText() {
        HomePage().checkHomePageHasText("This is home Fragment")
    }

    @Test
    fun checkDashboardPageHasText() {
        DashboardPage().checkDashboardPageHasText("This is dashboard Fragment")
    }

    @Test
    fun checkNotificationsPageHasText() {
        NotificationsPage().checkNotificationsPageHasText("This is notifications Fragment")
    }

}